# Habits Project

Dokumentacja jest zawarta w pliku "Projekt Zarządzanie Danymi W65550.pdf" w głównym katalogu repozytorium.

Author: Piotr Najda w65550

## Prereqisites

- yarn package manager
- supabase CLI installed

## Quickstart

```
yarn install

cd src && supabase db start

yarn dev
```

Reseeding database: `cd src && supabase db reset`

Swagger-ui dostępny jest pod ścieżką `localhost:3000/docs` po uruchomieniu aplikacji podążając za README.md repozytorium.
