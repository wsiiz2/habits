-- If no user, insert one 
DO $$
DECLARE 
  userId UUID := '3295afa0-fa4e-46a3-a583-2b214e210924';
  userId2 UUID := '1295afa0-fa4e-46a3-a583-2b214e210924';
BEGIN 

-- Generated from doing supabase.auth.signUp for creds:
-- test@example.com
-- password 
-- and then running select * from auth.users and using
-- https://konbert.com/convert/json/to/sql converter. 
INSERT INTO auth.users ("instance_id","id","aud","role","email","encrypted_password","email_confirmed_at","invited_at","confirmation_token","confirmation_sent_at","recovery_token","recovery_sent_at","email_change_token_new","email_change","email_change_sent_at","last_sign_in_at","raw_app_meta_data","raw_user_meta_data","is_super_admin","created_at","updated_at","phone","phone_confirmed_at","phone_change","phone_change_token","phone_change_sent_at","email_change_token_current","email_change_confirm_status","banned_until","reauthentication_token","reauthentication_sent_at","is_sso_user","deleted_at","is_anonymous")
VALUES
('00000000-0000-0000-0000-000000000000','3295afa0-fa4e-46a3-a583-2b214e210924','authenticated','authenticated','test@example.com','$2a$10$QphL877M8K4BxZo.JxXX/O0MXWpNn29m.xG1fw123tKwTTR924wfm','2024-06-28 07:53:54.666342+00',NULL,'',NULL,'',NULL,'','',NULL,'2024-06-28 07:53:54.672239+00','{"provider":"email","providers":["email"]}','{"sub":"3295afa0-fa4e-46a3-a583-2b214e210924","email":"test@example.com","email_verified":false,"phone_verified":false}',NULL,'2024-06-28 07:53:54.660947+00','2024-06-28 07:53:54.674247+00',NULL,NULL,'','',NULL,'',0,NULL,'',NULL,FALSE,NULL,FALSE);

-- test2@example.com
-- password
INSERT INTO auth.users ("instance_id","id","aud","role","email","encrypted_password","email_confirmed_at","invited_at","confirmation_token","confirmation_sent_at","recovery_token","recovery_sent_at","email_change_token_new","email_change","email_change_sent_at","last_sign_in_at","raw_app_meta_data","raw_user_meta_data","is_super_admin","created_at","updated_at","phone","phone_confirmed_at","phone_change","phone_change_token","phone_change_sent_at","email_change_token_current","email_change_confirm_status","banned_until","reauthentication_token","reauthentication_sent_at","is_sso_user","deleted_at","is_anonymous")
VALUES
('00000000-0000-0000-0000-000000000000','1295afa0-fa4e-46a3-a583-2b214e210924','authenticated','authenticated','test2@example.com','$2a$10$QphL877M8K4BxZo.JxXX/O0MXWpNn29m.xG1fw123tKwTTR924wfm','2024-06-28 07:53:54.666342+00',NULL,'',NULL,'',NULL,'','',NULL,'2024-06-28 07:53:54.672239+00','{"provider":"email","providers":["email"]}','{"sub":"1295afa0-fa4e-46a3-a583-2b214e210924","email":"test2@example.com","email_verified":false,"phone_verified":false}',NULL,'2024-06-28 07:53:54.660947+00','2024-06-28 07:53:54.674247+00',NULL,NULL,'','',NULL,'',0,NULL,'',NULL,FALSE,NULL,FALSE);

INSERT INTO habits (name, description, motivation, uid) VALUES 
('Exercise', 'Daily morning workout', 'Stay healthy and fit', userId),
('Read', 'Read books for personal development', 'Gain knowledge and relax', userId);

INSERT INTO habits (name, description, motivation, uid) VALUES 
('Swim', 'Daily morning swim', 'Stay healthy and fit', userId2),
('Meditate', 'Meditate for personal development', 'Gain knowledge and relax', userId2);

INSERT INTO habits_days (habit_id, day, from_time, to_time, uid) VALUES 
(1, 0, '07:00', '08:00', userId),
(1, 1, '07:00', '08:00', userId),
(2, 0, '20:00', '20:30', userId2);

INSERT INTO habits_to_categories (habit_id, category_id, uid) VALUES 
(1, 1, userId),
(1, 2, userId),
(2, 1, userId);

INSERT INTO habits_executions (habit_id, execution_date, execution_time, uid) VALUES 
(1, '2024-06-13', '07:00', userId),
(1, '2024-06-14', '07:00', userId),
(2, '2024-06-13', '20:00', userId),
(2, '2024-06-14', '20:00', userId);

END $$; 
