
CREATE TYPE habit_status AS ENUM ('active', 'paused', 'completed');

CREATE TABLE habits (
    id SERIAL PRIMARY KEY,
    uid UUID NOT NULL,
    name VARCHAR(255) NOT NULL,
    description TEXT,
    motivation TEXT,
    status habit_status DEFAULT 'active',
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (uid) REFERENCES auth.users(id) ON DELETE CASCADE
);

CREATE TABLE habits_categories (
    id SERIAL PRIMARY KEY,
    uid UUID NOT NULL,
    name VARCHAR(255) NOT NULL,
    FOREIGN KEY (uid) REFERENCES auth.users(id) ON DELETE CASCADE
);

-- #Create trigger to create default categories for each user
CREATE OR REPLACE FUNCTION create_default_categories()
RETURNS TRIGGER AS $$
BEGIN
    INSERT INTO habits_categories (uid, name)
    VALUES (NEW.id, 'Health'), (NEW.id, 'Personal Development');
    RETURN NEW;
END;
$$ LANGUAGE plpgsql security definer set search_path = public, admin, pg_temp;

CREATE TRIGGER create_default_categories_trigger
AFTER INSERT ON auth.users
FOR EACH ROW
EXECUTE FUNCTION create_default_categories();
-- /Create trigger to create default categories for each user


CREATE TABLE habits_days (
    id SERIAL PRIMARY KEY,
    habit_id INT,
    uid UUID NOT NULL,
    day INT CHECK (day BETWEEN 0 AND 6),
    -- If no from_time and to_time, it means somewhen in the day
    from_time TIME,
    to_time TIME,
    FOREIGN KEY (habit_id) REFERENCES habits(id) ON DELETE CASCADE,
    CHECK (from_time <= to_time),
    FOREIGN KEY (uid) REFERENCES auth.users(id) ON DELETE CASCADE
);

CREATE TABLE habits_to_categories (
    habit_id INT,
    category_id INT,
    uid UUID NOT NULL,
    PRIMARY KEY (habit_id, category_id),
    FOREIGN KEY (habit_id) REFERENCES habits(id) ON DELETE CASCADE,
    FOREIGN KEY (category_id) REFERENCES habits_categories(id) ON DELETE CASCADE,
    FOREIGN KEY (uid) REFERENCES auth.users(id) ON DELETE CASCADE
);


CREATE TABLE habits_executions (
    id SERIAL PRIMARY KEY,
    habit_id INT,
    uid UUID NOT NULL,
    execution_date DATE DEFAULT CURRENT_DATE CHECK (execution_date <= CURRENT_DATE),
    execution_time TIME DEFAULT CURRENT_TIME,
    FOREIGN KEY (habit_id) REFERENCES habits(id) ON DELETE CASCADE,
    FOREIGN KEY (uid) REFERENCES auth.users(id) ON DELETE CASCADE
);

