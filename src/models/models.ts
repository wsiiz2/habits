import z from "zod";

export const ID_PARAM_SCHEMA = z.object({
  params: z.object({
    id: z.string(),
  }),
});

export const PAGINATE_SCHEMA = z.object({
  limit: z.number({ coerce: true }).gte(1).max(100).default(10),
  offset: z.number({ coerce: true }).gte(0).default(0),
});

export const SORT_SCHEMA = z.object({
  sort: z.string().optional(),
  order: z.string().optional(),
});

export const CATEGORY_SCHEMA = z.object({
  id: z.number(),
  name: z.string().max(255),
});

export const CREATE_CATEGORY_SCHEMA = z.object({
  body: CATEGORY_SCHEMA.pick({
    name: true,
  }),
});

export const UPDATE_CATEGORY_SCHEMA = z.object({
  params: z.object({
    id: z.string(),
  }),
  body: CATEGORY_SCHEMA.pick({
    name: true,
  }),
});

export const DAY_SCHEMA = z.object({
  day: z.number().min(0).max(6),
  from_time: z.string(),
  to_time: z.string(),
});

export const EXECUTION_SCHEMA = z.object({
  id: z.number(),
  date: z.string(),
  time: z.string(),
});

export const HABIT_SCHEMA = z.object({
  created_at: z.number(),
  id: z.number(),
  name: z.string().max(255),
  description: z.string().optional(),
  motivation: z.string().optional(),
  status: z.enum(["active", "paused", "completed"]),
  categories: z.array(CATEGORY_SCHEMA),
  days: z.array(DAY_SCHEMA),
  executions: z.array(EXECUTION_SCHEMA),
});

export const CREATE_HABIT_SCHEMA = z.object({
  body: HABIT_SCHEMA.pick({
    name: true,
    description: true,
    motivation: true,
  }).and(
    z.object({
      categories: z
        .array(
          CATEGORY_SCHEMA.pick({
            id: true,
          })
        )
        .optional(),
      days: z.array(DAY_SCHEMA).optional(),
    })
  ),
});

export const UPDATE_HABIT_SCHEMA = z.object({
  params: z.object({
    id: z.string(),
  }),
  body: HABIT_SCHEMA.pick({
    name: true,
    description: true,
    motivation: true,
  })
    .optional()
    .and(
      z.object({
        categories: z
          .array(
            CATEGORY_SCHEMA.pick({
              id: true,
            })
          )
          .optional(),
        days: z.array(DAY_SCHEMA).optional(),
        status: HABIT_SCHEMA.shape.status.optional(),
      })
    ),
});

export const ADD_HABIT_EXECUTION_SCHEMA = z.object({
  params: z.object({
    habit_id: z.string(),
  }),
  body: z.object({
    date: EXECUTION_SCHEMA.shape.date.optional(),
    time: EXECUTION_SCHEMA.shape.time.optional(),
  }),
});
