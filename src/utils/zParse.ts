import type { Request } from "express";
import { AnyZodObject, ZodError, z } from "zod";

// https://dev.to/franciscomendes10866/schema-validation-with-zod-and-expressjs-111p
/**
 * BE Export
 */
export async function zParse<T extends AnyZodObject>(
  schema: T,
  req: Request
): Promise<z.infer<T>> {
  try {
    return await schema.parseAsync({
      body: req.body,
      query: req.query,
      params: req.params,
    });
  } catch (error) {
    if (error instanceof ZodError) {
      return Promise.reject({
        message: error.errors,
        status: 400,
      });
    }
    return Promise.reject({
      message: "An unknown error occurred",
      status: 500,
    });
  }
}

type ValidationError<T extends Record<string, unknown> = {}> = {
  message: string;
  path: string;
} & T;

// TODO?: Duplication with the above type?
/**
 * BE Export
 *
 * This can be extended to include payload data, e.g.:
 *
 * ```
 * const ExtendedSchema = VALIDATION_ERROR_SCHEMA.extend({
 *  someObject: z.array(...)
 * })
 * ```
 *
 * And then used in createValidationErrors:
 *
 * ```
 * createValidationErrors<z.infer<typeof ExtendedSchma>>(...)
 * ```
 */
export const VALIDATION_ERROR_SCHEMA = z.object({
  message: z.string(),
  path: z.string(),
});

/**
 * BE Export
 */
export const createValidationErrors = <
  T extends ValidationError = ValidationError
>(
  errors: T | T[]
): T[] => {
  if (!Array.isArray(errors)) {
    return [errors];
  }
  return errors;
};

/**
 * UI Export
 */
export const isValidationErrors = (
  error: unknown
): error is ValidationError[] => {
  if (!Array.isArray(error)) {
    return false;
  }
  return error.every((e) => VALIDATION_ERROR_SCHEMA.safeParse(e).success);
};

/**
 * BE Export
 *
 * Used in React hook form, when an error is on the root level.
 */
export const ROOT_PATH = "root";

// // import type { Request, Response, NextFunction } from "express";
// // import { AnyZodObject, ZodError, z } from "zod";

// // export async function zParse<T extends AnyZodObject>(
// //   schema: T,
// //   req: Request
// // ): Promise<z.infer<T>> {
// //   try {
// //     return await schema.parseAsync(req.body);
// //   } catch (error) {
// //     if (error instanceof ZodError) {
// //       return Promise.reject({
// //         message: error.errors,
// //         status: 400,
// //       });
// //     }
// //     return Promise.reject({
// //       message: "An unknown error occurred",
// //       status: 500,
// //     });
// //   }
// // }

// // The above version doesn't use a middleware. ref:
// // https://dev.to/franciscomendes10866/schema-validation-with-zod-and-expressjs-111p
// // and first comment there.

// import { Request, Response, NextFunction } from "express";
// import { AnyZodObject, z, ZodError } from "zod";
// export const zParse =
//   (schema: AnyZodObject) =>
//   async (req: Request, res: Response, next: NextFunction) => {
//     // To debug a 500, uncomment out the catch.
//     try {
//       // Optional pass in body, query, params (see link above)
//       await schema.parseAsync(req.body);
//       return next();
//     } catch (error) {
//       if (error instanceof ZodError) {
//         return res.status(400).json(
//           error.errors.map((e) => ({
//             message: e.message,
//             path: e.path.join("."),
//           }))
//         );
//       }
//       return res.status(500).json(error);
//     }
//   };

// export type ValidationError<T extends Record<string, unknown> = {}> = {
//   message: string;
//   path: string;
// } & T;

// // TODO: Duplication with the above?
// export const VALIDATION_ERROR_SCHEMA = z.object({
//   message: z.string(),
//   path: z.string(),
// });

// export const isValidationErrors = (
//   error: unknown
// ): error is ValidationError[] => {
//   if (!Array.isArray(error)) {
//     return false;
//   }
//   return error.every((e) => VALIDATION_ERROR_SCHEMA.safeParse(e).success);
// };

// export const newValidationErrors = <
//   T extends ValidationError = ValidationError
// >(
//   errors: T | T[]
// ): T[] => {
//   if (!Array.isArray(errors)) {
//     return [errors];
//   }
//   return errors;
// };
