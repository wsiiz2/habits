import express from "express";

const router = express.Router();

import habitController from "../controllers/habit.controller";
import { categoryController } from "../controllers/category.controller";

/**
 * Habits
 */

router.get("/habits", habitController.getList);
router.get("/habits/:id", habitController.getOne);
router.put("/habits/:id", habitController.updateOne);
router.post("/habits", habitController.createOne);

router.post("/habits/:habit_id/executions", habitController.addHabitExecution);
router.delete(
  "/habits/:habit_id/executions/:id",
  habitController.removeHabitExecution
);

router.get("/categories", categoryController.getList);
router.put("/categories/:id", categoryController.updateOne);
router.post("/categories", categoryController.createOne);
router.delete("/categories/:id", categoryController.deleteOne);

export default router;
