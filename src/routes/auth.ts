import { supabase } from "../app";
import express from "express";
import { createValidationErrors } from "../utils/zParse";

const router = express.Router();

router.post("/sign-up", async (req, res) => {
  const { email, password } = req.body;

  try {
    const {
      data: { user, session },
      error,
    } = await supabase.auth.signUp({
      email,
      password,
    });

    if (error) {
      return res.status(400).json({ error: error.message });
    }

    return res.status(201).json({ jwt: session?.access_token, user });
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ error: error.message });
    } else {
      return res.status(500).json({ error: "Unknown error" });
    }
  }
});

router.post("/sign-in", async (req, res) => {
  const { email, password } = req.body;

  try {
    const {
      data: { user, session },
      error,
    } = await supabase.auth.signInWithPassword({
      email,
      password,
    });

    if (error) {
      return res.status(400).json(
        createValidationErrors({
          path: "root",
          message: error.message,
        })
      );
    }

    return res.status(200).json({ jwt: session?.access_token, user });
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ error: error.message });
    } else {
      return res.status(500).json({ error: "Unknown error" });
    }
  }
});

router.post("/sign-out", async (req, res) => {
  const token = req.headers.authorization?.split(" ")[1];

  if (!token) {
    return res.status(400).json({ error: "Token is required" });
  }

  try {
    const { error } = await supabase.auth.admin.signOut(token, "global");

    if (error) {
      return res.status(400).json({ error: error.message });
    }

    return res.status(204).send();
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ error: error.message });
    } else {
      return res.status(500).json({ error: "Unknown error" });
    }
  }
});

export default router;
