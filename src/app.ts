import createError from "http-errors";
import express, { ErrorRequestHandler } from "express";
import path from "path";
import cookieParser from "cookie-parser";
import logger from "morgan";
import { createClient } from "@supabase/supabase-js";

import dotenv from "dotenv";

import indexRouter from "./routes";
import authRouter from "./routes/auth";

import cors from "cors";
import swaggerDocs from "./swagger";

import { createPool, DatabasePool } from "slonik";

import { createResultParserInterceptor } from "./utils/slonik-interceptor";
import expressAsyncHandler from "express-async-handler";

// Setup env file
dotenv.config();

const app = express();

// Express generator code
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

// Cors
app.use(
  cors({
    origin: "http://localhost:*",
  })
);

// Connect to database
async function main() {
  const pool = await createPool(
    "postgresql://postgres:postgres@127.0.0.1:54322/postgres",
    {
      interceptors: [createResultParserInterceptor()],
    }
  );
  console.log("Connected to database");
  db = pool;
}
main().catch((err) => console.log(err));

const SUPABASE_SERVICE_KEY = process.env.SUPABASE_SERVICE_KEY;

export const supabase = createClient(
  "http://127.0.0.1:54321",
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZS1kZW1vIiwicm9sZSI6InNlcnZpY2Vfcm9sZSIsImV4cCI6MTk4MzgxMjk5Nn0.EGIM96RAZx35lJzdJsyH-qQwv8Hdp7fsn3W0YpN81I"
);

export let db: DatabasePool;

// Slow down middleware in dev
if (process.env.NODE_ENV === "development") {
  app.use((req, res, next) => {
    if (process.env.SLOW_DOWN !== undefined) {
      setTimeout(next, parseInt(process.env.SLOW_DOWN, 10));
    }
  });
}

export const verifyJWT = expressAsyncHandler(async (req, res, next) => {
  const token = req.headers.authorization?.split(" ")[1];
  const { data } = await supabase.auth.getUser(token);
  if (!data.user) {
    res.status(401).json({ message: "Unauthorized" });
    return;
  }
  // Store user id in locals
  res.locals.uid = data.user?.id;
  next();
});

swaggerDocs(app, 3000);

app.use("/auth", authRouter);
app.use("/", verifyJWT, indexRouter);

app.listen(3000, () => {
  console.log("Server is running on http://localhost:3000");
});

// Express generator: Catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

const errorHandler: ErrorRequestHandler = (err, req, res, next) => {
  // Set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};
  res.status(err.status || 500);
  res.json(err);
};

// Express generator: Error handler
app.use(errorHandler);

export default app;
