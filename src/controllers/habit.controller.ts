import { RequestHandler } from "express";
import expressAsyncHandler from "express-async-handler";
import { db } from "../app";
import { sql } from "slonik";
import { zParse } from "../utils/zParse";
import {
  ID_PARAM_SCHEMA,
  HABIT_SCHEMA,
  CREATE_HABIT_SCHEMA,
  ADD_HABIT_EXECUTION_SCHEMA,
  UPDATE_HABIT_SCHEMA,
} from "../models/models";
import { z } from "zod";

const getList: RequestHandler = expressAsyncHandler(async (req, res) => {
  const query = sql.type(HABIT_SCHEMA)`
    select habits.*, 
    
    coalesce(json_agg(DISTINCT jsonb_build_object('id', hc.id, 'name', hc.name)) 
    FILTER (WHERE hc.id IS NOT NULL), '[]'
    )as categories,
    
    coalesce(json_agg(DISTINCT jsonb_build_object('day', hd.day, 'from_time', hd.from_time, 'to_time', hd.to_time)) 
    FILTER (WHERE hd.day IS NOT NULL), '[]') as days,
    
    coalesce(json_agg(DISTINCT jsonb_build_object('id', he.id,
    'date', he.execution_date, 'time', he.execution_time))
    FILTER (WHERE he.id IS NOT NULL), '[]')
    as executions

    from habits
    left join habits_to_categories htc on habits.id = htc.habit_id
    left join habits_categories hc on htc.category_id = hc.id
    left join habits_days hd on habits.id = hd.habit_id
    left join habits_executions he on habits.id = he.habit_id

    where habits.uid = ${sql.literalValue(res.locals.uid)}

    group by habits.id;
  `;

  const result = await db.any(query);

  res.json(result);
});

const getOne: RequestHandler = expressAsyncHandler(async (req, res) => {
  const { params } = await zParse(ID_PARAM_SCHEMA, req);

  const query = sql.type(HABIT_SCHEMA)`
    select habits.*, 
    
    coalesce(json_agg(DISTINCT jsonb_build_object('id', hc.id, 'name', hc.name)) 
    FILTER (WHERE hc.id IS NOT NULL), '[]'
    )as categories,
    
    coalesce(json_agg(DISTINCT jsonb_build_object('day', hd.day, 'from_time', hd.from_time, 'to_time', hd.to_time)) 
    FILTER (WHERE hd.day IS NOT NULL), '[]') as days,
    
    coalesce(json_agg(DISTINCT jsonb_build_object('id', he.id,
    'date', he.execution_date, 'time', he.execution_time))
    FILTER (WHERE he.id IS NOT NULL), '[]')
    as executions

    from habits
    left join habits_to_categories htc on habits.id = htc.habit_id
    left join habits_categories hc on htc.category_id = hc.id
    left join habits_days hd on habits.id = hd.habit_id
    left join habits_executions he on habits.id = he.habit_id

    where habits.uid = ${sql.literalValue(res.locals.uid)}
    AND habits.id = ${params.id}

    group by habits.id;
  `;

  const result = await db.maybeOne(query);

  if (!result) {
    res.status(404).json({ message: "Habit not found" });
    return;
  }

  res.json(result);
});

const createOne: RequestHandler = expressAsyncHandler(async (req, res) => {
  const { body } = await zParse(CREATE_HABIT_SCHEMA, req);

  const habitQuery = sql.type(
    z.object({
      id: z.number(),
    })
  )`
    insert into habits (name, description, motivation, uid)
    values (${body.name}, ${body.description || ""}, ${
    body.motivation || ""
  }, ${res.locals.uid})
    returning id;
  `;

  const result = await db.transaction(async (transactionConnection) => {
    const habitResult = await transactionConnection.one(habitQuery);

    // Construct categories query based on the habit id
    if (body.categories?.length) {
      const values = body.categories.map(
        (category) =>
          sql.fragment`(${habitResult.id}, ${category.id}, ${res.locals.uid})`
      );
      const queryCategories = sql.unsafe`
      insert into habits_to_categories (habit_id, category_id, uid)
      values ${sql.join(values, sql.fragment`, `)};
    `;
      await transactionConnection.query(queryCategories);
    }

    // Construct days query based on the habit id
    if (body.days?.length) {
      const values = body.days.map(
        (day) =>
          sql.fragment`(${habitResult.id}, ${day.day}, ${day.from_time}, ${day.to_time}, ${res.locals.uid})`
      );
      const queryDays = sql.unsafe`
      insert into habits_days (habit_id, day, from_time, to_time, uid)
      values ${sql.join(values, sql.fragment`, `)};
    `;
      await transactionConnection.query(queryDays).catch((err) => {
        console.log(err);
        throw err;
      });
    }

    return habitResult;
  });

  res.status(201).json({ id: result.id });
});

const updateOne: RequestHandler = expressAsyncHandler(async (req, res) => {
  const { body, params } = await zParse(UPDATE_HABIT_SCHEMA, req);

  const id = await db.transaction(async (tc) => {
    const query = sql.type(
      z.object({
        id: z.number(),
      })
    )`
    update habits 
    set name = ${body.name},
    description = ${body.description || ""},
    motivation = ${body.motivation || ""},
    status = ${body.status || "active"}
    where id = ${params.id} and uid = ${res.locals.uid}
    returning id;    
  `;

    const result = await tc.maybeOne(query);

    // Update categories
    if (body.categories) {
      const deleteExistingCategories = sql.unsafe`
      delete from habits_to_categories
      where habit_id = ${params.id} and uid = ${res.locals.uid};
    `;
      await tc.query(deleteExistingCategories);
    }
    if (body.categories?.length) {
      const values = body.categories.map(
        (category) =>
          sql.fragment`(${params.id}, ${category.id}, ${res.locals.uid})`
      );
      const queryCategories = sql.unsafe`
      insert into habits_to_categories (habit_id, category_id, uid)
      values ${sql.join(values, sql.fragment`, `)}
      on conflict (habit_id, category_id) do nothing;
    `;
      await tc.query(queryCategories);
    }

    // Update days
    if (body.days) {
      const deleteExistingDays = sql.unsafe`
      delete from habits_days
      where habit_id = ${params.id} and uid = ${res.locals.uid};
    `;
      await tc.query(deleteExistingDays);
    }

    if (body.days?.length) {
      const values = body.days.map(
        (day) =>
          sql.fragment`(${params.id}, ${day.day}, ${day.from_time}, ${day.to_time}, ${res.locals.uid})`
      );
      const queryDays = sql.unsafe`
      insert into habits_days (habit_id, day, from_time, to_time, uid)
      values ${sql.join(values, sql.fragment`, `)}
      on conflict (habit_id, day) do nothing;
    `;
      await tc.query(queryDays);
    }

    return result;
  });

  if (!id) {
    res.status(404).json({ message: "Habit not found" });
    return;
  }

  res.status(200).json({ id: id.id });
});

const addHabitExecution: RequestHandler = expressAsyncHandler(
  async (req, res) => {
    const { body, params } = await zParse(ADD_HABIT_EXECUTION_SCHEMA, req);

    const columns = sql.join(
      [
        sql.identifier(["habit_id"]),
        ...(body.date ? [sql.identifier(["execution_date"])] : []),
        ...(body.time ? [sql.identifier(["execution_time"])] : []),
        sql.identifier(["uid"]),
      ],
      sql.fragment`, `
    );

    const query = sql.type(
      z.object({
        id: z.number(),
      })
    )`
    insert into habits_executions (${columns})
    values (${sql.join(
      [
        params.habit_id,
        ...(body.date ? [body.date] : []),
        ...(body.time ? [body.time] : []),
        res.locals.uid,
      ],
      sql.fragment`, `
    )})
    returning id;
  `;

    const result = await db.one(query).catch((err) => {
      console.log(err);
      throw err;
    });

    res.status(201).json({ id: result.id });
  }
);

const removeHabitExecution: RequestHandler = expressAsyncHandler(
  async (req, res) => {
    const { params } = await zParse(
      z.object({
        params: z.object({
          habit_id: z.string(),
          id: z.string(),
        }),
      }),
      req
    );

    const query = sql.type(
      z.object({
        id: z.number(),
      })
    )`
    delete from habits_executions
    where id = ${params.id} and uid = ${res.locals.uid}
    returning id;
  `;

    const result = await db.maybeOne(query);

    if (!result) {
      res.status(404).json({ message: "Execution not found" });
      return;
    }

    res.json({ id: result.id });
  }
);

export default {
  getList,
  getOne,
  createOne,
  updateOne,
  addHabitExecution,
  removeHabitExecution,
};
