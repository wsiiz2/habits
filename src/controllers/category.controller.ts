import { RequestHandler } from "express";
import expressAsyncHandler from "express-async-handler";
import {
  CATEGORY_SCHEMA,
  CREATE_CATEGORY_SCHEMA,
  ID_PARAM_SCHEMA,
  UPDATE_CATEGORY_SCHEMA,
} from "../models/models";
import { zParse } from "../utils/zParse";
import { sql } from "slonik";
import { z } from "zod";
import { db } from "../app";

export const getList: RequestHandler = expressAsyncHandler(async (req, res) => {
  const query = sql.type(CATEGORY_SCHEMA)`
    select * from habits_categories
    where uid = ${res.locals.uid};
  `;

  const result = await db.any(query);

  res.json(result);
});

export const createOne: RequestHandler = expressAsyncHandler(
  async (req, res) => {
    const { body } = await zParse(CREATE_CATEGORY_SCHEMA, req);

    const alreadyExistsQuery = sql.unsafe`select 1 from habits_categories where name = ${body.name}`;

    const alreadyExists = await db.maybeOne(alreadyExistsQuery).catch((err) => {
      console.log(err);
      throw err;
    });

    console.log(alreadyExists);

    if (alreadyExists) {
      res.status(409).json({ error: "Category already exists" });
      return;
    }

    const query = sql.type(
      z.object({
        id: z.number(),
      })
    )`
    insert into habits_categories (name, uid)
    values (${body.name}, ${res.locals.uid})
    returning id;
  `;

    const result = await db.one(query);

    res.status(201).json({ id: result.id });
  }
);

export const updateOne: RequestHandler = expressAsyncHandler(
  async (req, res) => {
    const { params, body } = await zParse(UPDATE_CATEGORY_SCHEMA, req);

    const query = sql.type(
      z.object({
        id: z.number(),
      })
    )`
    update habits_categories
    set
      name = ${body.name}
    where id = ${params.id}
    and uid = ${res.locals.uid}
    returning id;
  `;

    const result = await db.maybeOne(query);

    if (!result) {
      res.status(404).json({ error: "Category not found" });
      return;
    }

    res.status(200).json({ id: result.id });
  }
);

const deleteOne: RequestHandler = expressAsyncHandler(async (req, res) => {
  const { params } = await zParse(ID_PARAM_SCHEMA, req);

  const query = sql.unsafe`
  delete from habits_categories
  where id = ${params.id}
  and uid = ${res.locals.uid}
  returning id;
`;

  const result = await db.maybeOne(query);

  if (!result) {
    res.status(404).json({ error: "Category not found" });
    return;
  }

  res.status(200).json({ id: result.id });
});

export const categoryController = {
  getList,
  updateOne,
  createOne,
  deleteOne,
};
