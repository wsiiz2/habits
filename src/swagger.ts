import { Express, Request, Response } from "express";
import swaggerUi from "swagger-ui-express";
import YAML from "yamljs";
import fs from "fs";
import path from "path";

function getSwaggerdDoc() {
  const swagger_path = path.join(__dirname, "swagger.yaml");
  const file = fs.readFileSync(swagger_path, "utf8");
  return YAML.parse(file);
}

function swaggerDocs(app: Express, port: number) {
  // Swagger page
  app.use("/docs", swaggerUi.serve, swaggerUi.setup(getSwaggerdDoc()));
  // Docs in JSON format
  app.get("/docs.json", (req: Request, res: Response) => {
    res.setHeader("Content-Type", "application/json");
    res.send(getSwaggerdDoc());
  });
  console.log(`Docs available at http://localhost:${port}/docs`);
}

export default swaggerDocs;
